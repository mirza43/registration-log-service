package com.precorconnect.registrationlogservice.identityservice;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccessContext;
import com.precorconnect.AppAccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerRepAccessContext;
import com.precorconnect.registrationlogservice.IdentityServiceAdapter;

@Singleton
public class IdentityServiceAdapterImpl implements
        IdentityServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public IdentityServiceAdapterImpl(
            @NonNull IdentityServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
    public AccessContext getAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetAccessContextFeature.class)
                        .execute(accessToken);

    }

    @Override
    public AppAccessContext getAppAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetAppAccessContextFeature.class)
                        .execute(accessToken);

    }

    @Override
    public PartnerRepAccessContext getPartnerRepAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetPartnerRepAccessContextFeature.class)
                        .execute(accessToken);

    }
}
