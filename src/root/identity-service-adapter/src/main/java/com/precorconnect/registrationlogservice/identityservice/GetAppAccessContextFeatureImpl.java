package com.precorconnect.registrationlogservice.identityservice;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.AppAccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class GetAppAccessContextFeatureImpl
        implements GetAppAccessContextFeature {

    /*
    fields
     */
    private final IdentityServiceSdk identityServiceSdk;

    /*
    constructors
     */
    @Inject
    public GetAppAccessContextFeatureImpl(
            @NonNull IdentityServiceSdk identityServiceSdk
    ) {

    	this.identityServiceSdk =
                guardThat(
                        "identityServiceSdk",
                         identityServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public AppAccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return identityServiceSdk
                .getAppAccessContext(
                        accessToken
                );

    }
}
