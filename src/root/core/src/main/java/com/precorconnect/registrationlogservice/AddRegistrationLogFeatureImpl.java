package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

@Singleton
class AddRegistrationLogFeatureImpl
        implements AddRegistrationLogFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public AddRegistrationLogFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public PartnerSaleRegistrationId execute(
            @NonNull AddPartnerSaleRegistrationLog request
    ) {

        return
                databaseAdapter
                        .addRegistrationLog(
                                request
                        );

    }
}
