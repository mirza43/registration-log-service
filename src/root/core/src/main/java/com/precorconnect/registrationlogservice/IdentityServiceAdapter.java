package com.precorconnect.registrationlogservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccessContext;
import com.precorconnect.AppAccessContext;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerRepAccessContext;

public interface IdentityServiceAdapter {
	
	 AccessContext getAccessContext(
	            @NonNull OAuth2AccessToken accessToken
	    ) throws AuthenticationException;

	    PartnerRepAccessContext getPartnerRepAccessContext(
	            @NonNull OAuth2AccessToken accessToken
	    ) throws AuthenticationException;

	    AppAccessContext getAppAccessContext(
	            @NonNull OAuth2AccessToken accessToken
	    ) throws AuthenticationException;

}
