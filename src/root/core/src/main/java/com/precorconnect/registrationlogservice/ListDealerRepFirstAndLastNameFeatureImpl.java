package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;

public class ListDealerRepFirstAndLastNameFeatureImpl implements ListDealerRepFirstAndLastNameFeature {
	
	 /*
    fields
     */
   private final DatabaseAdapter databaseAdapter;

   /*
   constructors
    */
   @Inject
   public ListDealerRepFirstAndLastNameFeatureImpl(
           @NonNull DatabaseAdapter databaseAdapter
   ) {

   	this.databaseAdapter =
               guardThat(
                       "databaseAdapter",
                        databaseAdapter
               )
                       .isNotNull()
                       .thenGetValue();

   }

	@Override
	public Collection<RegistrationDealerRepView> execute(
			@NonNull List<PartnerSaleRegistrationId> registrationIds) {
		return 
				databaseAdapter.listDealerRepFirstAndLastName(registrationIds);
	}

}
