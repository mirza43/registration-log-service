package com.precorconnect.registrationlogservice;


import static org.assertj.core.api.StrictAssertions.assertThat;

import org.junit.Test;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;

public class AddPartnerSaleRegistrationLogImplTest {
	
	/** fields
     */
    private final Dummy dummy = new Dummy();
    
    /*
    test methods
     */    
    @Test(expected = IllegalArgumentException.class)
    public void constructorPartnerSaleRegistrationIdOverride_NullName_Throws(
    ) throws Exception {

        new AddPartnerSaleRegistrationLogImpl(
                null,
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }

    @Test
    public void constructorPartnerSaleRegistrationIdOverride_SetsId(
    ) throws Exception {

        /*
        arrange
         */
        PartnerSaleRegistrationId expectedId = dummy.getPartnerSaleRegistrationId();
        
        /*
        act
         */
        AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
        /*
        assert
         */
        PartnerSaleRegistrationId actualId = objectUnderTest.getPartnerSaleRegistrationId();

        assertThat(actualId)
                .isEqualTo(expectedId);

    }

    
    @Test(expected = IllegalArgumentException.class)
    public void constructorAccountIdOverride_NullAccountId_Throws(
    ) throws Exception {
    	
    	
        new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                null,
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }
    
    @Test
    public void constructorAccountIdOverride_setAccountId(
    ) throws Exception {
    	
    	AccountId expected = dummy.getAccountId();
    	
    	AddPartnerSaleRegistrationLog  objectUnderTest =  new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
        /*
        assert
         */
    	AccountId actual = objectUnderTest.getAccountId();

        assertThat(actual)
                .isEqualTo(expected);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructorAccountNameOverride_NullAccountName_Throws(
    ) throws Exception {
    	
    
        new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                null,
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }
    
    
    @Test
    public void constructorAccountNameOverride_setAccountName(
    ) throws Exception {
    	
    	AccountName expected = dummy.getAccountName();
    
    	AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
    	AccountName actual = objectUnderTest.getAccountName();

        assertThat(actual)
                .isEqualTo(expected);

    }
   
    
    @Test(expected = IllegalArgumentException.class)
    public void constructorSellDateOverride_NullSellDate_Throws(
    ) throws Exception {
    	
    
        new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                null,
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }
    
    
    @Test
    public void constructorSellDateOverride_setSellDate(
    ) throws Exception {
    	
    	SellDate expected = dummy.getSellDate();
    
    	AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
    	SellDate actual = objectUnderTest.getSellDate();

        assertThat(actual)
                .isEqualTo(expected);

    }
     
    
    @Test
    public void constructorInstallDateOverride_setInstallDate(
    ) throws Exception {
    	
    	InstallDate expected = dummy.getInstallDate();
    
    	AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
    	InstallDate actual = objectUnderTest.getInstallDate().get();

        assertThat(actual)
                .isEqualTo(expected);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructorSubmittedDateOverride_NullSubmittedDate_Throws(
    ) throws Exception {
    	
    
        new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                null,
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }
    
    
    @Test
    public void constructorSubmittedDateOverride_setSubmittedDate(
    ) throws Exception {
    	
    	SubmittedDate expected = dummy.getSubmittedDate();
    
    	AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
    	SubmittedDate actual = objectUnderTest.getSubmittedDate();

        assertThat(actual)
                .isEqualTo(expected);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructorEWStatusOverride_NullEWStatus_Throws(
    ) throws Exception {
    	
    
        new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                null,
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }
    
    
    @Test
    public void constructorEWStatusOverride_setEWStatus(
    ) throws Exception {
    	
    	ExtendedWarrantyStatus expected = dummy.getEWStatus();
    
    	AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
    	ExtendedWarrantyStatus actual = objectUnderTest.getExtendedWarrantyStatus();

        assertThat(actual)
                .isEqualTo(expected);

    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructorSpiffStatussOverride_NullSpiffStatus_Throws(
    ) throws Exception {
    	
    
        new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                null,
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );

    }
    
    
    @Test
    public void constructorSpiffStatusOverride_setSpiffStatus(
    ) throws Exception {
    	
    	SpiffStatus expected = dummy.getSpiffStatus();
    
    	AddPartnerSaleRegistrationLog  objectUnderTest = new AddPartnerSaleRegistrationLogImpl(
                dummy.getPartnerSaleRegistrationId(),
                dummy.getAccountId(),
                dummy.getAccountName(),
                dummy.getSellDate(),
                dummy.getInstallDate(),
                dummy.getSubmittedDate(),
                dummy.getEWStatus(),
                dummy.getSpiffStatus(),
                dummy.getFirstName(),
                dummy.getLastName(),
                dummy.getEmailAddress(),
                dummy.getUserId(),
                dummy.getSubmittedByName()
        );
        
    	SpiffStatus actual = objectUnderTest.getSpiffStatus();

        assertThat(actual)
                .isEqualTo(expected);

    }
   
}
