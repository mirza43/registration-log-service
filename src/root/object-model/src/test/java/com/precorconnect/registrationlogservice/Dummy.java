package com.precorconnect.registrationlogservice;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.AccountName;
import com.precorconnect.AccountNameImpl;
import com.precorconnect.EmailAddress;
import com.precorconnect.EmailAddressImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private AccountId accountId = new AccountIdImpl("001K000001H2Km2IAF");
       
    private final PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(1L);
    
    private final AccountName accountName =new AccountNameImpl("CucumberTest");
    
    private final SellDate sellDate = new SellDateImpl(Instant.now());

	private final InstallDate installDate = new InstallDateImpl(Instant.now());
	
	private final SubmittedDate submittedDate = new SubmittedDateImpl(Instant.now());
	
	/*
	 If the partnerSaleRegistrationId is greater than 1M (i.e from Consumer Sale) 
	 then EW Status is "Not Applicable"
	 */
	private final ExtendedWarrantyStatus EWStatus = new ExtendedWarrantyStatusImpl("Not Submitted");
	
	private final SpiffStatus spiffStatus = new SpiffStatusImpl("Not Submitted");
	
	private final FirstName firstName = new FirstNameImpl("firstName");
	
	private final LastName lastName = new LastNameImpl("lastName");
	
	private final EmailAddress emailAddress = new EmailAddressImpl("aa@gmail.com");
	
	private final UserId userId = new UserIdImpl("00u7bu9dkoYhYoqyk0h7");
	
	private final AccountName submittedByName =new AccountNameImpl("James Van");
    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
        
    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

	public AccountId getAccountId() {
		return accountId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public AccountName getAccountName() {
		return accountName;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SubmittedDate getSubmittedDate() {
		return submittedDate;
	}

	public ExtendedWarrantyStatus getEWStatus() {
		return EWStatus;
	}

	public SpiffStatus getSpiffStatus() {
		return spiffStatus;
	}

	public FirstName getFirstName() {
		return firstName;
	}

	public LastName getLastName() {
		return lastName;
	}

	public EmailAddress getEmailAddress() {
		return emailAddress;
	}

	public UserId getUserId() {
		return userId;
	}

	public AccountName getSubmittedByName() {
		return submittedByName;
	}

}
