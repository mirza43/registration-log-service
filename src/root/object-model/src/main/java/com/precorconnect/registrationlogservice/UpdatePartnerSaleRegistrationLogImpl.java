package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class UpdatePartnerSaleRegistrationLogImpl 
		implements UpdatePartnerSaleRegistrationLog {
	
	private PartnerSaleRegistrationId partnerSaleRegistrationId;
	
	private SpiffStatus spiffStatus;
	
	public UpdatePartnerSaleRegistrationLogImpl(
            @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,            
            @NonNull SpiffStatus spiffStatus
    ) {

        this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

        this.spiffStatus =
                guardThat(
                        "spiffStatus",
                        spiffStatus
                )
                        .isNotNull()
                        .thenGetValue();
	}

	@Override
	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public SpiffStatus getSpiffStatus() {
		return spiffStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0
						: partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((spiffStatus == null) ? 0 : spiffStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdatePartnerSaleRegistrationLogImpl other = (UpdatePartnerSaleRegistrationLogImpl) obj;
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null)
				return false;
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId))
			return false;
		if (spiffStatus == null) {
			if (other.spiffStatus != null)
				return false;
		} else if (!spiffStatus.equals(other.spiffStatus))
			return false;
		return true;
	}
	
	

}
