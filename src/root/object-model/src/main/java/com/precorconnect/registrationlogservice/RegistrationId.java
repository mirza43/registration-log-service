package com.precorconnect.registrationlogservice;

public interface RegistrationId {

	Long getValue();
}
