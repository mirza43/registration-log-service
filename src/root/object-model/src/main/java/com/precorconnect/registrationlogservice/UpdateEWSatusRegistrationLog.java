package com.precorconnect.registrationlogservice;


public interface UpdateEWSatusRegistrationLog {
	
	PartnerSaleRegistrationId getPartnerSaleRegistrationId();
	
	ExtendedWarrantyStatus getExtendedWarrantyStatus();

}
