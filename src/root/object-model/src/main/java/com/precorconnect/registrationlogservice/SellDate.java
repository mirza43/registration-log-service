package com.precorconnect.registrationlogservice;

import java.time.Instant;

public interface SellDate {

	Instant getValue();
}
