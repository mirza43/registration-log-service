package com.precorconnect.registrationlogservice;

import java.time.Instant;

public interface InstallDate {

	Instant getValue();
}
