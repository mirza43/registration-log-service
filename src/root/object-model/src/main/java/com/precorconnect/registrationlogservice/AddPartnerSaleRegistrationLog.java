package com.precorconnect.registrationlogservice;

import java.util.Optional;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;
import com.precorconnect.EmailAddress;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public interface AddPartnerSaleRegistrationLog {
	
	PartnerSaleRegistrationId getPartnerSaleRegistrationId();
	
	AccountId getAccountId();
	
	AccountName getAccountName();
	
	Optional<InstallDate> getInstallDate();

    SellDate getSellDate();
    
    SubmittedDate getSubmittedDate();
    
    ExtendedWarrantyStatus getExtendedWarrantyStatus();
    
    SpiffStatus getSpiffStatus();
    
    FirstName getFirstName();
	
	LastName getLastName();
	
	EmailAddress getEmailAddress();
	
	UserId getUserId();
	
	AccountName getSubmittedByName();

}
