package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;

public class RegistrationLogViewImpl 
				implements RegistrationLogView{

	private final RegistrationId registrationId;
	private final PartnerSaleRegistrationId partnerSaleRegistrationId;
	private final AccountId accountId;
	private final SellDate sellDate;
	private final InstallDate installDate;
	private final SubmittedDate submittedDate;
	private final FacilityName facilityName;
	private final ExtendedWarrantyStatus extendedWarrantyStatus;
	private final SpiffStatus spiffStatus;
	
	public RegistrationLogViewImpl(
			@NonNull RegistrationId registrationId,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull AccountId accountId,
			@NonNull SellDate sellDate,
			@Nullable InstallDate installDate,
			@NonNull SubmittedDate submittedDate,
			@NonNull FacilityName facilityName,
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull SpiffStatus spiffStatus
			) {

		 
		this.registrationId =
                guardThat(
                        "registrationId",
                        registrationId
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.accountId =
                guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate = installDate;
                

		this.submittedDate =
                guardThat(
                        "submittedDate",
                        submittedDate
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();
		
		
		this.extendedWarrantyStatus =
                guardThat(
                        "extendedWarrantyStatus",
                        extendedWarrantyStatus
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffStatus =
                guardThat(
                        "spiffStatus",
                        spiffStatus
                )
                        .isNotNull()
                        .thenGetValue();

	}
	
	public RegistrationId getRegistrationId() {
		return registrationId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public AccountId getAccountId() {
		return accountId;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	public Optional<InstallDate> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	public SubmittedDate getSubmittedDate() {
		return submittedDate;
	}

	public FacilityName getFacilityName() {
		return facilityName;
	}

	public ExtendedWarrantyStatus getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}

	public SpiffStatus getSpiffStatus() {
		return spiffStatus;
	}
	
}
