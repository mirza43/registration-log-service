package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;

public class RegistrationDealerRepViewImpl implements RegistrationDealerRepView {
	
	private PartnerSaleRegistrationId registrationId;

	private FirstName firstName;

	private LastName lastName;

	public RegistrationDealerRepViewImpl(
			@NonNull  PartnerSaleRegistrationId registrationId,
			@Nullable FirstName firstName,
			@Nullable LastName lastName) {
		
		this.registrationId =
                guardThat(
                        "registrationId",
                        registrationId
                		)
                .isNotNull()
                .thenGetValue();

		this.firstName = firstName;
		
		this.lastName = lastName;
	}

	@Override
	public FirstName getFirstName() {
		return firstName;
	}

	@Override
	public LastName getLastName() {
		return lastName;
	}

	@Override
	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return registrationId;
	}

}
