package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class UpdateEWSatusRegistrationLogImpl implements UpdateEWSatusRegistrationLog {
	
	private PartnerSaleRegistrationId partnerSaleRegistrationId;
	
	private ExtendedWarrantyStatus extendedWarrantyStatus;
	
	
	
	public UpdateEWSatusRegistrationLogImpl(
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,            
            @NonNull ExtendedWarrantyStatus extendedWarrantyStatus){
		
		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

        this.extendedWarrantyStatus =
                guardThat(
                        "extendedWarrantyStatus",
                        extendedWarrantyStatus
                )
                        .isNotNull()
                        .thenGetValue();
		
	}

	@Override
	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public ExtendedWarrantyStatus getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((extendedWarrantyStatus == null) ? 0
						: extendedWarrantyStatus.hashCode());
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0
						: partnerSaleRegistrationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateEWSatusRegistrationLogImpl other = (UpdateEWSatusRegistrationLogImpl) obj;
		if (extendedWarrantyStatus == null) {
			if (other.extendedWarrantyStatus != null)
				return false;
		} else if (!extendedWarrantyStatus.equals(other.extendedWarrantyStatus))
			return false;
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null)
				return false;
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId))
			return false;
		return true;
	}
	
	
}
