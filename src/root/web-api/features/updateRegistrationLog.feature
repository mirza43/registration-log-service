Feature: Update Registration Log 
	updates a registration log with spiff status
	
	Background:
    Given an updateRegistrationLog consists of:
      | attribute    				 | validation | type   |
      | partnerSaleRegistrationId    | required   | Long   |
      | spiffStatus                  | required   | string |
      
      
   Scenario: Success
    Given I provide an accessToken identifying me as <identity>
    And provide a valid updateRegistrationLog
    When I PUT to /updateRegistrationLog
    Then a registration log is updates the registration-log table with spiff status for given partner sale registration id