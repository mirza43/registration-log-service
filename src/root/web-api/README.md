## Description
Precor Connect Registration Log service API for Rest.

## Features

#####Add Registration Log
* [documentation](features/addRegistrationLog.feature)

#####List Registration Log
* [documentation](features/listRegistrationLog.feature)

#####Update Registration Log Status
* [documentation](features/updateRegistrationLog.feature)


## API Explorer

##### Environments:
-  [dev](https://dev.precorconnect.com/)
-  [prod](https://precorconnect.com/)