package com.precorconnect.registrationlogservice.webapi;

import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;

public interface ListUpdateRegistrationLogFactory {
	
	UpdatePartnerSaleRegistrationLog construct(UpdateRegistrationLog updateRegistrationLog);

}
