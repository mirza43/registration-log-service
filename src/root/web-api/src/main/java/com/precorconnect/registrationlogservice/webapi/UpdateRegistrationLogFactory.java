package com.precorconnect.registrationlogservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateRegistrationLogFactory {
	
	com.precorconnect.registrationlogservice.UpdateEWSatusRegistrationLog construct(
			com.precorconnect.registrationlogservice.webapi.@NonNull UpdateEWSatusRegistrationLog updateEWSatusRegistrationLog
			);

}
