package com.precorconnect.registrationlogservice.webapi;




import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

@Component
class JacksonJdk8ModuleBean {

    @Bean
    @Singleton
    public Jdk8Module jdk8Module() {

        return new Jdk8Module();

    }
}
