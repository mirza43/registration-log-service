package com.precorconnect.registrationlogservice.webapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.AccountName;
import com.precorconnect.AccountNameImpl;
import com.precorconnect.EmailAddress;
import com.precorconnect.EmailAddressImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLogImpl;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatusImpl;
import com.precorconnect.registrationlogservice.InstallDate;
import com.precorconnect.registrationlogservice.InstallDateImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.SellDate;
import com.precorconnect.registrationlogservice.SellDateImpl;
import com.precorconnect.registrationlogservice.SpiffStatus;
import com.precorconnect.registrationlogservice.SpiffStatusImpl;
import com.precorconnect.registrationlogservice.SubmittedDate;
import com.precorconnect.registrationlogservice.SubmittedDateImpl;

@Component
public class AddRegistrationLogFactoryImpl 
			implements AddRegistrationLogFactory {

	@Override
	public AddPartnerSaleRegistrationLog construct(
			@NonNull AddRegistrationLog request
			) {
		
		PartnerSaleRegistrationId partnerSaleRegistrationId = 
				new PartnerSaleRegistrationIdImpl(
						request.getPartnerSaleRegistrationId()
						);
		
		AccountId accountId = 
				new AccountIdImpl(
						request.getPartnerAccountId()
						);
		
		AccountName accountName = 
				new AccountNameImpl(
						request.getAccountName()
						);
		
		SellDate sellDate = null;
		InstallDate installDate = null;
		SubmittedDate submittedDate = null;
		
		try {
		 sellDate = 
				new SellDateImpl(
					 new SimpleDateFormat("MM/dd/yyyy")
					 	.parse(request.getSellDate())
					 	.toInstant()
				);
		
		installDate =
				request.getInstallDate().isPresent()?
				new InstallDateImpl(
						new SimpleDateFormat("MM/dd/yyyy")
						.parse(request.getInstallDate().get())
						.toInstant()
				)
				:null;
		
		submittedDate = 
				new SubmittedDateImpl(
						new SimpleDateFormat("MM/dd/yyyy")
						.parse(request.getSubmittedDate())
						.toInstant()
				);
		}catch(ParseException e){
			e.printStackTrace();
		}
		
		ExtendedWarrantyStatus extendedWarrantyStatus = 
				new ExtendedWarrantyStatusImpl(
						request.getExtendedWarrantyStatus()
						);
		
		SpiffStatus spiffStatus = 
				new SpiffStatusImpl("Not Submitted");
		
		Optional<FirstName> firstName = request.getFirstName().map(FirstNameImpl::new);
		
		Optional<LastName> lastName = request.getLastName().map(LastNameImpl::new);
		
		Optional<EmailAddress> emailAddress = request.getEmail().map(EmailAddressImpl::new);
		
		Optional<UserId> userId = request.getPartnerRepId().map(UserIdImpl::new);
		
		Optional<AccountName> submittedByName = request.getSubmittedByName().map(AccountNameImpl::new);
		
		return 
				new AddPartnerSaleRegistrationLogImpl(
						partnerSaleRegistrationId, 
						accountId, 
						accountName, 
						sellDate, 
						installDate, 
						submittedDate, 
						extendedWarrantyStatus,
						spiffStatus,
						firstName.orElse(null),
						lastName.orElse(null),
						emailAddress.orElse(null),
						userId.orElse(null),
						submittedByName.orElse(null)
						);
	
	}

}
