package com.precorconnect.registrationlogservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.RegistrationDealerRepView;

public interface ListRegistrationDealerRepFactory {
	
	RegistrationDealerRepSynopsisView construct(
			@NonNull RegistrationDealerRepView registrationDealerRepView
			);

}
