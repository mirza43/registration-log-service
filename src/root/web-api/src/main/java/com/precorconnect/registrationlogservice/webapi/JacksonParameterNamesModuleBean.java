package com.precorconnect.registrationlogservice.webapi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.inject.Singleton;

@Component
class JacksonParameterNamesModuleBean {

    @Bean
    @Singleton
    public ParameterNamesModule parameterNamesModule() {

        return new ParameterNamesModule(JsonCreator.Mode.PROPERTIES);

    }
}
