package com.precorconnect.registrationlogservice.webapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ContextConfiguration;

import com.precorconnect.registrationlogservice.webapi.Application;

@ContextConfiguration(classes = Application.class, loader = SpringApplicationContextLoader.class)
@WebIntegrationTest(randomPort = true)
public abstract class AbstractSpringIntegrationTest {

	@Value("${local.server.port}")
    private String port;

    protected Integer getPort(){
        return Integer.parseInt(port);
    }
}
