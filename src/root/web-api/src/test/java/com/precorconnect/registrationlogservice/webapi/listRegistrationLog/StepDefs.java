package com.precorconnect.registrationlogservice.webapi.listRegistrationLog;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.registrationlogservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.registrationlogservice.webapi.Config;
import com.precorconnect.registrationlogservice.webapi.ConfigFactory;
import com.precorconnect.registrationlogservice.webapi.Dummy;
import com.precorconnect.registrationlogservice.webapi.Factory;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs extends AbstractSpringIntegrationTest {
	
	 private final Config config =
	            new ConfigFactory().construct();

	    private final Dummy dummy = new Dummy();

	    private final Factory factory =
	            new Factory(
	                    dummy,
	                    new IdentityServiceIntegrationTestSdkImpl(
	                            config.getIdentityServiceJwtSigningKey()
	                    )
	            );

	    private OAuth2AccessToken accessToken;
	    
	    private AccountId accountId;
	    
	    private Response response;

	    
	    @Before
	    public void beforeAll() {

	        RestAssured.port = getPort();

	    }
	    
	    @Given("^an addRegistrationLog consists of:$")
	    public void an_addRegistrationLog_consists_of(DataTable arg1) throws Throwable {
	        // No op Account Id from Dummy
	    }

	    @Given("^I provide an accessToken identifying me as <identity>$")
	    public void i_provide_an_accessToken_identifying_me_as_identity() throws Throwable {
	        
	    	accessToken = new OAuth2AccessTokenImpl(
					factory
						.constructValidPartnerRepOAuth2AccessToken()
						.getValue()
					);
	    }

	    @Given("^provide a valid AccountId$")
	    public void provide_a_valid_AccountId() throws Throwable {
	        
	    	accountId = new AccountIdImpl(dummy.getAccountId());
	    }

	    @When("^I GET to /AccountId$")
	    public void i_GET_to_AccountId() throws Throwable {
	       
	    	response =
	    			given()
	                .contentType(ContentType.JSON)
	                .header(
	                        "Authorization",
	                        String.format(
	                                "Bearer %s",
	                                accessToken
	                                        .getValue()
	                        )
	                )
	                .get("/registration-log/"+accountId.getValue());
	    }

	    @Then("^list of registration log is details which are associated to Account Id fetch from registration-log table$")
	    public void list_of_registration_log_is_details_which_are_associated_to_Account_Id_fetch_from_registration_log_table() throws Throwable {
	    	
	    	response
				.then()
				.assertThat()
				.statusCode(200);
	    }


}
