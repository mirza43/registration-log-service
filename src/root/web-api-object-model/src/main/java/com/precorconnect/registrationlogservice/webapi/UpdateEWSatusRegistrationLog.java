package com.precorconnect.registrationlogservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class UpdateEWSatusRegistrationLog {
	
	private final Long partnerSaleRegistrationId;
	
	private final String ewStatus;
	
	
	
	public UpdateEWSatusRegistrationLog(
    		@NonNull Long partnerSaleRegistrationId,
    		@NonNull String ewStatus
    		) {

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                		)
                        .isNotNull()
                        .thenGetValue();

		this.ewStatus =
                guardThat(
                        "ewStatus",
                        ewStatus
                		)
                        .isNotNull()
                        .thenGetValue();
		
		
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getEwStatus() {
		return ewStatus;
	}

	

}
