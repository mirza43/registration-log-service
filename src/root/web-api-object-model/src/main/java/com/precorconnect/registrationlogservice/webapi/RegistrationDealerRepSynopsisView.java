package com.precorconnect.registrationlogservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class RegistrationDealerRepSynopsisView {

	private final long registrationId;
	
	private final String firstName;
	
	private final String lastName;
	

	public RegistrationDealerRepSynopsisView(
			@NonNull long registrationId,  
			@Nullable String firstName,
			@Nullable String lastName) {
		
		this.registrationId =
				guardThat(
						"registrationId",
						registrationId
						)
						.isNotNull()
						.thenGetValue();
		
		this.firstName = firstName;
		
		this.lastName = lastName;
		
		
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public long getRegistrationId() {
		return registrationId;
	}
	
	

}
