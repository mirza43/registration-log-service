package com.precorconnect.registrationlogservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class AddRegistrationLog {
	
	private final Long partnerSaleRegistrationId;
	
	private final String partnerAccountId;
	
	private final String accountName;
	
	private final String sellDate;

	private final String installDate;
	
	private final String submittedDate;
	
	private final String extendedWarrantyStatus;
	
	private final String firstName;
	
	private final String lastName;
	
	private final String email;
	
	private final String partnerRepId;
	
	private final String submittedByName;
	
	public AddRegistrationLog(
    		@NonNull Long partnerSaleRegistrationId,
    		@NonNull String partnerAccountId,
    		@NonNull String accountName,
    		@NonNull String sellDate,
    		@Nullable String installDate,
    		@NonNull String submittedDate,
    		@NonNull String extendedWarrantyStatus,
    		@Nullable String firstName,
    		@Nullable String lastName,
    		@Nullable String email,
    		@Nullable String partnerRepId,
    		@Nullable String submittedByName
    		) {

    	this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                		)
                .isNotNull()
                .thenGetValue();

		this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                        partnerAccountId
                		)
                .isNotNull()
                .thenGetValue();

		this.accountName =
                guardThat(
                        "accountName",
                        accountName
                		)
                .isNotNull()
                .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                		)
                .isNotNull()
                .thenGetValue();

		this.installDate = installDate;

		this.submittedDate =
                guardThat(
                        "submittedDate",
                        submittedDate
                		)
                .isNotNull()
                .thenGetValue();
		
		this.extendedWarrantyStatus = 
				guardThat(
						"extendedWarrantyStatus", 
						extendedWarrantyStatus
						)
				.isNotNull()
                .thenGetValue();
		
		this.firstName = firstName;
		
		this.lastName = lastName;
		
		this.email = email;
		
		this.partnerRepId = partnerRepId;
		
		this.submittedByName = submittedByName;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getPartnerAccountId() {
		return partnerAccountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public String getSellDate() {
		return sellDate;
	}

	public Optional<String> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	public String getSubmittedDate() {
		return submittedDate;
	}

	public String getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}
	
	public Optional<String> getEmail() {
		return Optional.ofNullable(email);
	}

	public Optional<String> getFirstName() {
		return Optional.ofNullable(firstName);
	}

	public Optional<String> getLastName() {
		return Optional.ofNullable(lastName);
	}
	
	public Optional<String> getPartnerRepId() {
		return Optional.ofNullable(partnerRepId);
	}
	
	public Optional<String> getSubmittedByName() {
		return Optional.ofNullable(submittedByName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountName == null) ? 0 : accountName.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime
				* result
				+ ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		result = prime
				* result
				+ ((partnerSaleRegistrationId == null) ? 0 : partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result
				+ ((submittedDate == null) ? 0 : submittedDate.hashCode());
		result = prime * result
				+ ((extendedWarrantyStatus == null) ? 0 : extendedWarrantyStatus.hashCode());
		result = prime * result
				+ ((partnerRepId == null) ? 0 : partnerRepId.hashCode());
		return result;
	}

	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddRegistrationLog other = (AddRegistrationLog) obj;
		if (accountName == null) {
			if (other.accountName != null)
				return false;
		} else if (!accountName.equals(other.accountName))
			return false;
		if (installDate == null) {
			if (other.installDate != null)
				return false;
		} else if (!installDate.equals(other.installDate))
			return false;
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null)
				return false;
		} else if (!partnerAccountId.equals(other.partnerAccountId))
			return false;
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null)
				return false;
		} else if (!partnerSaleRegistrationId
				.equals(other.partnerSaleRegistrationId))
			return false;
		if (sellDate == null) {
			if (other.sellDate != null)
				return false;
		} else if (!sellDate.equals(other.sellDate))
			return false;
		if (submittedDate == null) {
			if (other.submittedDate != null)
				return false;
		} else if (!submittedDate.equals(other.submittedDate))
			return false;
		if (extendedWarrantyStatus == null) {
			if (other.extendedWarrantyStatus != null)
				return false;
		} else if (!extendedWarrantyStatus.equals(other.extendedWarrantyStatus))
			return false;
		if (partnerRepId == null) {
			if (other.partnerRepId != null)
				return false;
		} else if (!partnerRepId.equals(other.partnerRepId))
			return false;
		return true;
	}
	
	
}
