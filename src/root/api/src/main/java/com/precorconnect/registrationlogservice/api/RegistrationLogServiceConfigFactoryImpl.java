package com.precorconnect.registrationlogservice.api;

import com.precorconnect.registrationlogservice.database.DatabaseAdapterConfigFactoryImpl;
import com.precorconnect.registrationlogservice.identityservice.IdentityServiceAdapterConfigFactoryImpl;

public class RegistrationLogServiceConfigFactoryImpl 
				implements RegistrationLogServiceConfigFactory {

	@Override
	public RegistrationLogServiceConfig construct() {
		
		return 
				new RegistrationLogServiceConfigImpl(
						new DatabaseAdapterConfigFactoryImpl()
												  .construct(),
						new IdentityServiceAdapterConfigFactoryImpl()
													.construct()
					);
		
				
	}

}
