package com.precorconnect.registrationlogservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.EmailAddress;
import com.precorconnect.EmailAddressImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.SapVendorNumberImpl;
import com.precorconnect.UserIdImpl;
import com.precorconnect.identityservice.AppJwt;
import com.precorconnect.identityservice.AppJwtImpl;
import com.precorconnect.identityservice.PartnerRepJwt;
import com.precorconnect.identityservice.PartnerRepJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdk;

public class Factory {

    /*
    fields
     */
    private final Dummy dummy;

    private final IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk;

    /*
    constructors
     */
    public Factory(
            @NonNull Dummy dummy,
            @NonNull IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk
    ) {

    	this.dummy =
                guardThat(
                        "dummy",
                         dummy
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceIntegrationTestSdk =
                guardThat(
                        "identityServiceIntegrationTestSdk",
                         identityServiceIntegrationTestSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
        factory methods
         */
    public OAuth2AccessToken constructValidAppOAuth2AccessToken() {

        AppJwt appJwt =
                new AppJwtImpl(
                        Instant.now().plus(Duration.ofMinutes(10)),
                        dummy.getUri(),
                        dummy.getUri()
                );

        return identityServiceIntegrationTestSdk
                .getAppOAuth2AccessToken(appJwt);
    }

    public OAuth2AccessToken constructValidPartnerRepOAuth2AccessToken() {

    	PartnerRepJwt partnerRepJwt =
                new PartnerRepJwtImpl(
                        Instant.now().plusSeconds(86400),
                        dummy.getUri(),
                        dummy.getUri(),
                        new FirstNameImpl("venkataramana"),
                        new LastNameImpl("P"),
                        new UserIdImpl("00u5h8rhbtzPEtCH20h7"),
                        new AccountIdImpl("001K000001H2Km2IAF"),
                        new SapVendorNumberImpl("0000000000")
                );

    	return identityServiceIntegrationTestSdk
		.getPartnerRepOAuth2AccessToken(partnerRepJwt);
    }

    public EmailAddress constructUniqueEmailAddress() {
        return
                new EmailAddressImpl(
                        String.format(
                                "%s@q.com",
                                UUID.randomUUID().toString().replaceAll("-", "")
                        )
                );
    }

    public RegistrationLogServiceSdkConfig constructRegistrationLogServiceSdkConfig(
            Integer port
    ) {

        URL precorConnectApiBaseUrl;

        try {

        	precorConnectApiBaseUrl = new URL(String.format("http://localhost:%s", port));

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

        return
                new RegistrationLogServiceSdkConfigImpl(
                		precorConnectApiBaseUrl
                );

    }

}
