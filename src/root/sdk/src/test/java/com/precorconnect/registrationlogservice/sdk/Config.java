package com.precorconnect.registrationlogservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;

public class Config {

    /*
    fields
     */
    private final HmacKey identityServiceJwtSigningKey;

  

    /*
    constructors
     */
    public Config(
            final @NonNull HmacKey identityServiceJwtSigningKey
    ) {


        this.identityServiceJwtSigningKey =
                guardThat(
                        "identityServiceJwtSigningKey",
                        identityServiceJwtSigningKey
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
    */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }


}
