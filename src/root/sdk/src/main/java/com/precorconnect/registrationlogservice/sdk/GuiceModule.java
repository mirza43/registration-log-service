package com.precorconnect.registrationlogservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final RegistrationLogServiceSdkConfig config;

    /*
    constructors
     */
    @Inject
    public GuiceModule(
            @NonNull final RegistrationLogServiceSdkConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure(
    ) {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

    }

    private void bindFeatures() {

    	bind(AddRegistrationLogFeature.class)
    		.to(AddRegistrationLogFeatureImpl.class);
    	
    	bind(UpdateRegistrationLogFeature.class)
			.to(UpdateRegistrationLogFeatureImpl.class);
    	
    	bind(UpdateEWStatusForRegistrationLogFeature.class)
		.to(UpdateEWStatusForRegistrationLogFeatureImpl.class);

    }

    @Provides
    @Singleton
    WebTarget constructWebTarget(
            @NonNull final ObjectMapperProvider objectMapperProvider
    ) {

    	 try {
         	ClientConfig clientConfig = new ClientConfig();
         	clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, new PoolingHttpClientConnectionManager());
         	clientConfig.connectorProvider(new ApacheConnectorProvider());

             return
                     ClientBuilder
                             .newClient(clientConfig)
                             .property(ClientProperties.CONNECT_TIMEOUT, 3000)
                             .property(ClientProperties.READ_TIMEOUT,    10000)
                             .register(objectMapperProvider)
                             .register(JacksonFeature.class)
                             .target(config.getPrecorConnectApiBaseUrl().toURI());

         } catch (URISyntaxException e) {

             throw new RuntimeException(e);

         }
    }

}
