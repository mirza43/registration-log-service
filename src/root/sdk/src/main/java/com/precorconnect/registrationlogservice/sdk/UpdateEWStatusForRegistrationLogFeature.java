package com.precorconnect.registrationlogservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.webapi.UpdateEWSatusRegistrationLog;

public interface UpdateEWStatusForRegistrationLogFeature {
	
	void execute(
			@NonNull UpdateEWSatusRegistrationLog updateEWSatusRegistrationLog,
			@NonNull OAuth2AccessToken accessToken
    )throws AuthenticationException;

}
