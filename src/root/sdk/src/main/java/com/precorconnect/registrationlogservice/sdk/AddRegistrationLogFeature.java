package com.precorconnect.registrationlogservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

public interface AddRegistrationLogFeature {
	
	Long execute(
			@NonNull AddRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;

}
