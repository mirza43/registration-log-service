package com.precorconnect.registrationlogservice.sdk;

import java.net.URL;


public interface RegistrationLogServiceSdkConfig {
	
	URL getPrecorConnectApiBaseUrl();

}
