package com.precorconnect.registrationlogservice.sdk;


import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

public class AddRegistrationLogFeatureImpl implements AddRegistrationLogFeature {
	
	private final WebTarget baseWebTarget;
	
	@Inject
	public AddRegistrationLogFeatureImpl( @NonNull WebTarget baseWebTarget) {
		
		this.baseWebTarget = guardThat(
                "baseWebTarget",
                baseWebTarget
        )
                .isNotNull()
                .thenGetValue()
                .path("registration-log/addRegistrationLog");
	}

	@Override
	public Long execute(
			@NonNull AddRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
		
		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );
		
		Long partnerSaleRegistrationId = baseWebTarget
			.request(MediaType.APPLICATION_JSON_TYPE)
			.header("Authorization", authorizationHeaderValue)
			.post(Entity.entity(
                                		request,
                                        MediaType.APPLICATION_JSON
                                ),
                               Long.class
			);
		return partnerSaleRegistrationId;
	}

}
