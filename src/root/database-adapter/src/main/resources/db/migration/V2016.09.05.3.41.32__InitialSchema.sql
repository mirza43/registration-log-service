CREATE TABLE `RegistrationLog` (
  `Id` bigint(30) NOT NULL AUTO_INCREMENT,
  `PartnerSaleRegistrationId` bigint(30) NOT NULL,
   PartnerAccountId CHAR(18) NOT NULL,
  `sellDate` DATE NOT NULL,
  `installDate` DATE,
  `submittedDate` DATE NOT NULL,
  `facilityName` varchar(100) NOT NULL,
  `extendedWarrantyStatus` varchar(100) NOT NULL,
  `spiffStatus` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
);