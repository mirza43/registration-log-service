package com.precorconnect.registrationlogservice.database;

import java.util.Date;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;

@Singleton
public class PartnerRegistrationLogFactoryImpl 
			implements PartnerRegistrationLogFactory {

	@Override
	public RegistrationLog construct(
			@NonNull AddPartnerSaleRegistrationLog request,
			@NonNull RegistrationLog entityObject
			) {
		
		RegistrationLog registrationLog = entityObject;
		
		registrationLog.setPartnerSaleRegistrationId(request.getPartnerSaleRegistrationId().getValue());
		
		registrationLog.setPartnerAccountId(request.getAccountId().getValue());
		
		registrationLog.setFacilityName(request.getAccountName().getValue());
	
		registrationLog.setSellDate(new Date(request.getSellDate().getValue().toEpochMilli()));
		
		if(request.getInstallDate().isPresent()){
			registrationLog
				.setInstallDate(
						new Date(
								request.getInstallDate()
										.get()
										.getValue()
										.toEpochMilli())
						);
		}
		
		registrationLog.setSubmittedDate(new Date(request.getSubmittedDate().getValue().toEpochMilli()));
		
		registrationLog.setExtendedWarrantyStatus(request.getExtendedWarrantyStatus().getValue());
		
		registrationLog.setSpiffStatus(request.getSpiffStatus().getValue());
		registrationLog.setFirstName(request.getFirstName()==null? null : request.getFirstName().getValue());
		registrationLog.setLastName(request.getLastName()==null? null : request.getLastName().getValue());
		registrationLog.setEmail(request.getEmailAddress()==null? null : request.getEmailAddress().getValue());
		registrationLog.setPartnerRepId(request.getUserId()==null? null : request.getUserId().getValue());
		registrationLog.setSubmittedByName(request.getSubmittedByName()==null? null : request.getSubmittedByName().getValue());
		return registrationLog;
	}

}
