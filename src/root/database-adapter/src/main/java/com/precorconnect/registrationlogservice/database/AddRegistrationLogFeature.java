package com.precorconnect.registrationlogservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;

public interface AddRegistrationLogFeature {
	
	PartnerSaleRegistrationId execute(
	            @NonNull AddPartnerSaleRegistrationLog request
	    );

}
