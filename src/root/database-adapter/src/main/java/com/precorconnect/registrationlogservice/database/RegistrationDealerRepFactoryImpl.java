package com.precorconnect.registrationlogservice.database;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.RegistrationDealerRepView;
import com.precorconnect.registrationlogservice.RegistrationDealerRepViewImpl;

@Singleton
public class RegistrationDealerRepFactoryImpl 
				implements RegistrationDealerRepFactory {

	@Override
	public RegistrationDealerRepView construct(
			@NonNull RegistrationLog registrationLog) {
		
		FirstName firstName = new FirstNameImpl(registrationLog.getFirstName());
		
		LastName lastName = new LastNameImpl(registrationLog.getLastName());
		
		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(registrationLog.getPartnerSaleRegistrationId());
		
		return new RegistrationDealerRepViewImpl(partnerSaleRegistrationId, firstName, lastName);
	}

}
