package com.precorconnect.registrationlogservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.UpdateEWSatusRegistrationLog;

public interface UpdateEWStatusForRegistrationLogFeature {
	
	void execute(
			@NonNull UpdateEWSatusRegistrationLog updateEWSatusRegistrationLog
    );


}
