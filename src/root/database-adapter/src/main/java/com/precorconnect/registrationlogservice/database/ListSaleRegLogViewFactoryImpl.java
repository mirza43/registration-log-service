package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatusImpl;
import com.precorconnect.registrationlogservice.FacilityName;
import com.precorconnect.registrationlogservice.FacilityNameImpl;
import com.precorconnect.registrationlogservice.InstallDate;
import com.precorconnect.registrationlogservice.InstallDateImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.RegistrationId;
import com.precorconnect.registrationlogservice.RegistrationIdImpl;
import com.precorconnect.registrationlogservice.RegistrationLogView;
import com.precorconnect.registrationlogservice.RegistrationLogViewImpl;
import com.precorconnect.registrationlogservice.SellDate;
import com.precorconnect.registrationlogservice.SellDateImpl;
import com.precorconnect.registrationlogservice.SpiffStatus;
import com.precorconnect.registrationlogservice.SpiffStatusImpl;
import com.precorconnect.registrationlogservice.SubmittedDate;
import com.precorconnect.registrationlogservice.SubmittedDateImpl;

@Singleton
public class ListSaleRegLogViewFactoryImpl 
				implements ListRegistrationLogViewFactory{

	@Override
	public RegistrationLogView construct(
			@NonNull RegistrationLog registrationLog) {

		guardThat(
				"registrationLog", 
				registrationLog
				).isNotNull();
		
		RegistrationId registrationId = 
				new RegistrationIdImpl(
						registrationLog.getId()
						);
		
		
		PartnerSaleRegistrationId partnerSaleRegistrationId = 
				new PartnerSaleRegistrationIdImpl(
						registrationLog.getPartnerSaleRegistrationId()
						);
		
		AccountId accountId = 
				new AccountIdImpl(
						registrationLog.getPartnerAccountId()
						);
		
		SellDate sellDate = 
				new SellDateImpl(
						registrationLog.getSellDate().toInstant()
						);
		
		InstallDate installDate =
				registrationLog
					.getInstallDate().isPresent()?
						new InstallDateImpl(
						registrationLog.getInstallDate().get().toInstant()
						)
						:null;
		 
		SubmittedDate submittedDate = 
				new SubmittedDateImpl(
						registrationLog
							.getSubmittedDate()
							.toInstant()
							);
		
		FacilityName facilityName = 
				new FacilityNameImpl(
						registrationLog.getFacilityName()
						);
		
		ExtendedWarrantyStatus extendedWarrantyStatus = 
				new ExtendedWarrantyStatusImpl(
						registrationLog.getExtendedWarrantyStatus()
						);
		
		SpiffStatus spiffStatus = 
				new SpiffStatusImpl(
						registrationLog.getSpiffStatus()
						);
		
		return 
				new RegistrationLogViewImpl(
					registrationId,
					partnerSaleRegistrationId,
					accountId,
					sellDate,
					installDate,
					submittedDate,
					facilityName,
					extendedWarrantyStatus,
					spiffStatus
					);
				
	}
	
}
