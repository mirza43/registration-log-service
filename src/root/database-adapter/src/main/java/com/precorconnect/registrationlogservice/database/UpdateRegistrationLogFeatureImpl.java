package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;

public class UpdateRegistrationLogFeatureImpl
			implements UpdateRegistrationLogFeature {

	 /*
    fields
     */
    private final SessionFactory sessionFactory;

    /*
    constructors
     */
    @Inject
    public UpdateRegistrationLogFeatureImpl(
            @NonNull SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    public void execute(
            @NonNull List<UpdatePartnerSaleRegistrationLog> request
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            try {

                session.beginTransaction();
                
                for(UpdatePartnerSaleRegistrationLog updatePartnerSaleRegistrationLog : request) {

                Query query = session.createQuery("update RegistrationLog set spiffStatus= :spiffStatus"
                    						+" where partnerSaleRegistrationId= :partnerSaleRegistrationId");
                
                query.setParameter("spiffStatus", updatePartnerSaleRegistrationLog.getSpiffStatus().getValue());
            	query.setParameter("partnerSaleRegistrationId", updatePartnerSaleRegistrationLog.getPartnerSaleRegistrationId().getValue());
            	
            	query.executeUpdate();
                }
                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }

}
