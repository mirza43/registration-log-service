package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.RegistrationDealerRepView;

@Singleton
public class ListDealerRepFirstAndLastNameFeatureImpl
			implements ListDealerRepFirstAndLastNameFeature {

	/*
    fields
     */
    private final SessionFactory sessionFactory;

    private final RegistrationDealerRepFactory registrationDealerRepFactory;

    /*
    constructors
     */
    @Inject
    public ListDealerRepFirstAndLastNameFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull RegistrationDealerRepFactory registrationDealerRepFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

        this.registrationDealerRepFactory =
                guardThat(
                        "registrationDealerRepFactory",
                        registrationDealerRepFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<RegistrationDealerRepView> execute(
			@NonNull List<PartnerSaleRegistrationId> registrationIds
			) {
		
		Session session = null;

        try {

            session = sessionFactory.openSession();

            Query query = session.createQuery("from RegistrationLog rl where rl.partnerSaleRegistrationId in(:listRegistrationIds) ORDER BY rl.partnerSaleRegistrationId DESC");
            
        	query.setParameterList("listRegistrationIds", registrationIds.stream().map(repId -> repId.getValue()).collect(Collectors.toList()));

        	List<RegistrationLog> registrationLogs = query.list();

        	return registrationLogs
                    .stream()
                    .map(registrationDealerRepFactory::construct)
                    .collect(Collectors.toList());

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
		

}
