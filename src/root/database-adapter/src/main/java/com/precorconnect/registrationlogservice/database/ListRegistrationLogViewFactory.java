package com.precorconnect.registrationlogservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.RegistrationLogView;

public interface ListRegistrationLogViewFactory {
	
	RegistrationLogView construct(
	            @NonNull RegistrationLog registrationLog
	    );
}
