package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.registrationlogservice.UpdateEWSatusRegistrationLog;

@Singleton
public class UpdateEWStatusForRegistrationLogFeatureImpl implements UpdateEWStatusForRegistrationLogFeature {

		
	 /*
    fields
     */
    private final SessionFactory sessionFactory;

    /*
    constructors
     */
    @Inject
    public UpdateEWStatusForRegistrationLogFeatureImpl(
            @NonNull SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }
    
    
    @Override
	public void execute(@NonNull UpdateEWSatusRegistrationLog updateEWSatusRegistrationLog
			) {
    	
		  Session session = null;

	        try {

	            session = sessionFactory.openSession();

	            try {

	                session.beginTransaction();
	                
	              

	                Query query = session.createQuery("update RegistrationLog set extendedWarrantyStatus= :extendedWarrantyStatus "
	                    						+" where partnerSaleRegistrationId= :partnerSaleRegistrationId");
	                
	                query.setParameter("extendedWarrantyStatus", updateEWSatusRegistrationLog.getExtendedWarrantyStatus().getValue());
	            	query.setParameter("partnerSaleRegistrationId", updateEWSatusRegistrationLog.getPartnerSaleRegistrationId().getValue());
	            	query.executeUpdate();
	                
	                session.getTransaction().commit();

	            } catch (HibernateException e) {

	                session.getTransaction().rollback();
	                throw e;

	            }

	        } finally {

	            if (null != session) {
	                session.close();
	            }

	        }

	}

}
